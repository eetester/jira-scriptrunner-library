package com.elm.scriptrunner.scripts

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.EditableFieldLayout;
import com.atlassian.jira.project.Project
import org.apache.log4j.Logger


//*** Get Needed Managers ***
FieldManager fieldManager = ComponentAccessor.getFieldManager();
FieldLayoutManager layoutManager = ComponentAccessor.getComponent(FieldLayoutManager)
def log = Logger.getLogger("com.onresolve.jira.groovy")

//*** Initialize Variables ***
String result = "Done";
String projKey = "CAM"; //JIRA project Key

def usedPenTestFields = ["user role",
        "Complain On - Role",
        "Committee Actions",
        "PACSI Task",
        "Team Member Role",
        "Approvers",
        "Target PKey",
        "Target PName",
        "Projects List",
        "Project Category",
        "Employment Type",
        "Agreement",
        "Department",
        "Support Category",
        "Request Type",
        "Failure Reason",
        "Request Specific Type",
        "Contribution template",
        "Repository Name",
        "Java Version",
        "GIT Repo Link",
        "Maven Version",
        "Request User",
        "DotNet Version",
        "Plan Linlk",
        "Node Version",
        "Investigation reason"
]

Project p = ComponentAccessor.getProjectManager().getProjectObjByKey(projKey)

def fieldLayouts = layoutManager.getUniqueFieldLayouts(p)
def listOfCF = []
for (layout in fieldLayouts){
    if (layout.getName() != null){
        log.info "LAYOUT: ${layout.name}"
        def layoutId = layout.getId();
        EditableFieldLayout editableLayout = layoutManager.getEditableFieldLayout(layoutId);
        for (fieldLayoutItem in layout.getFieldLayoutItems()){
            //We won't touch system fields
            boolean isCF = fieldManager.isCustomField(fieldLayoutItem.getOrderableField().getId());
            if (isCF == true) {
                CustomField field = fieldManager.getCustomField(fieldLayoutItem.getOrderableField().getId());
                FieldLayoutItem item = editableLayout.getFieldLayoutItem(field);
                //editableLayout.show(item);
                if (layout.name == "Service Desk") {
                    if (usedPenTestFields.contains(fieldLayoutItem.getOrderableField().getName())) {
                        //if(item.isHidden() == true){
                        editableLayout.show(item);
                        //}
//                        log.info(item)
                    }
//                    listOfCF << field.name
                } else if (usedPenTestFields.contains(fieldLayoutItem.getOrderableField().getName())) {
                    editableLayout.hide(item);
//                    log.info(item)
                }
            }
        }
        layoutManager.storeEditableFieldLayout(editableLayout)
    }
}
return result;