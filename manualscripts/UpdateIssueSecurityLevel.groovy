package com.elm.scriptrunner.manualscripts

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.IssueInputParameters
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Logger
import com.atlassian.jira.web.bean.PagerFilter


def log = Logger.getLogger("com.onresolve.jira.groovy")
def userManager = ComponentAccessor.getUserManager()
def issueService = ComponentAccessor.issueService
def user = userManager.getUserByName("mmojahed")
def jqlSearch = "project = \"Health Specialties\""
def issues = findIssues(jqlSearch,user)

//def sprint = sprintServiceOutcome.getValue().find {it.name == "Mojaz Mobile Sprint 16"}
//Update fixversion & to update any other filed play with the issue set functions

for (issue in issues){
    def issueManager = ComponentAccessor.getIssueManager()
//    def versionManager = ComponentAccessor.getVersionManager()
    //def versions = versionManager.getVersionsByName("0.0.1.R")
    issue.setSecurityLevelId(10501L)//Update fixVersion Value
    //ImportantUpdate Issue for Lower level doesn't require any permission
    issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
}
//Get Issue List
def static findIssues(String jqlSearch,ApplicationUser user) {
    def searchService = ComponentAccessor.getComponent(SearchService)
    def issueManager = ComponentAccessor.getIssueManager()
    SearchService.ParseResult parseResult =  searchService.parseQuery(user, jqlSearch)
    def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
    def results = searchResult.results.collect {issueManager.getIssueObject(it.id)}
    return results
}