package com.elm.scriptrunner.scripts

import com.atlassian.jira.bc.projectroles.ProjectRoleService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.security.roles.ProjectRoleActor
import com.atlassian.jira.security.roles.ProjectRoleManager
import com.atlassian.jira.util.SimpleErrorCollection
import org.apache.log4j.Logger

def projectManager = ComponentAccessor.getProjectManager()
def projectRoleService = ComponentAccessor.getComponent(ProjectRoleService)
def projectRoleManager = ComponentAccessor.getComponent(ProjectRoleManager)
def errorCollection = new SimpleErrorCollection()
def log = Logger.getLogger("com.onresolve.jira.groovy")

def projectlist = projectManager.getProjects()

def project = projectManager.getProjectObjByKey("PROJKEY").toString().toLowerCase()
def projectRoles = projectRoleManager.getProjectRoles()

//def actors = ["obenouezdou","nelsarsoubi","ssikkandar","maltayeb"]
Collection<String> actorCollection = new ArrayList<>()
def actorGroups = ["Production_problem","Operation Mgrs","Performance_team","Security_Team","dev_audit"]

List rt =[]

for (pName in projectlist) {
    if (pName.projectCategory.name.contains('Archived')) {
        rt << pName.key
        for (projectRole in projectRoles) {
            def allActors = projectRoleService.getProjectRoleActors(projectRole, pName, errorCollection)
//    if (pName.projectCategory?.name == 'DigitalCustomSolutions' || 'Product Suites') {
            projectRoleService.removeActorsFromProjectRole(actorGroups, projectRole, pName, ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE, errorCollection)
            for (actor in allActors) {
                def listofUsers = actor.users
                listofUsers.each { it ->
                    projectRoleService.removeActorsFromProjectRole([it.username], projectRole, pName, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, errorCollection)
                }
            }
        }
    }
}
return rt.size()
