package com.elm.scriptrunner.manualscripts

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.web.bean.PagerFilter
import groovy.transform.Field
import org.apache.log4j.Logger

@Field log = Logger.getLogger("com.onresolve.jira.groovy")
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()


def jqlSearch="project = Tamm AND  component is EMPTY AND type != Change"

def issueKeys = findIssues(jqlSearch,user)


issueKeys.each {

    MutableIssue issue = ComponentAccessor.issueManager.getIssueByCurrentKey(it)
    Project project = issue.getProjectObject()
    def componentManager = ComponentAccessor.projectComponentManager
    def component = componentManager.findByComponentName(issue.projectObject.id, 'Old_Tamm')
    componentManager.updateIssueProjectComponents(issue, [component])

}



def static findIssues(String jqlSearch, ApplicationUser user) {
    def searchService = ComponentAccessor.getComponent(SearchService)
    def issueManager = ComponentAccessor.getIssueManager()
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
      def results = searchResult.results.key
   // def results = searchResult.results
    return results
}