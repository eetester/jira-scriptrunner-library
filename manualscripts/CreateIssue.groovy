package com.elm.scriptrunner.manualscripts

import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueInputParameters
import com.atlassian.jira.user.ApplicationUser

IssueService issueService = ComponentAccessor.getIssueService()
IssueInputParameters issueInputParameters = issueService.newIssueInputParameters();
def issue = ComponentAccessor.getIssueManager().getIssueByCurrentKey("ZAW-1085")
issueInputParameters
    .setProjectId( issue.getProjectObject().getId() )
    .setSummary( issue.getSummary() + issue.getFixVersions().first())
    .setDescription("Request to Perform Penteration Test for " + issue.getFixVersions().first())
    .setIssueTypeId('10403')
    .setReporterId(issue.reporterId)
    .setAssigneeId(issue.assigneeId)
    .setFixVersionIds(issue.getFixVersions().first().id)


ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

IssueService.CreateValidationResult createValidationResult =
    issueService.validateCreate(user, issueInputParameters)

if (createValidationResult.isValid())
{
    //log.error("entrou no createValidationResult")
    IssueService.IssueResult createResult = issueService.create(
        user, createValidationResult)
    log.debug(createResult)
    if (!createResult.isValid())
    {
        log.error("Error while creating the issue."  + createResult.errorCollection)
    }
}

