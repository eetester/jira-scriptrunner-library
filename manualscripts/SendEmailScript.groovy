package com.elm.scriptrunner.scripts

import com.elm.scriptrunner.library.SendJiraEmail
import com.onresolve.scriptrunner.runner.customisers.WithPlugin
import groovy.transform.Field
import org.apache.log4j.Logger


@Field def log = Logger.getLogger("com.atlassian.sclogs")
@WithPlugin("com.elm.jira.jira-library")

@Field SendJiraEmail hRM
hRM = new SendJiraEmail()


def emailbody =
        "<body>" +
                "<h3>This to inform you that a Project: '' is created in WIKI:</h3>" +
                "<p> you can access the projec by url: https://wiki.elm.sa/display/LEP/LEP </p>" +
                "<p> If you cannot access the project, please raise a service request: https://jira.elm.sa/servicedesk/customer/portal/1/create/10 </p>" +
                "</br>" +
                "</br>" +
                "<p>Regards,</p>" +
                "<p>Tech Affairs ALM Support | techsupport@elm.sa,</p>" +

                "</body>"


hRM.sendEmailtoUsers('muahmad@elm.sa',"", 'Test', emailbody)