package com.elm.scriptrunner.scripts

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.user.util.UserManager
import com.atlassian.jira.web.bean.PagerFilter
import org.apache.log4j.Logger
import org.apache.log4j.Level

def log = Logger.getLogger("com.onresolve.jira.groovy")
log.setLevel(Level.DEBUG)

def changeHistoryManager = ComponentAccessor.getChangeHistoryManager()
def searchProvider = ComponentAccessor.getComponent(SearchService)
def userManager = ComponentAccessor.getUserManager() as UserManager
def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
log.debug(user)
def jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser.class)
def jqlQuery = "issuetype = Bug"
def query = jqlQueryParser.parseQuery(jqlQuery)
def issue = ComponentAccessor.issueManager.getIssueByCurrentKey("CAE-89")

def results = searchProvider.search(user,query,PagerFilter.getUnlimitedFilter())
def issueList = null
LinkedHashMap listOfHistory = []


for (element in results.issues) {
    issueList = changeHistoryManager.getChangeItemsForField(element, "issuetype")
       if (issueList.find { it -> it.toString == 'Bug'}) {
            listOfHistory.put(element.key,issueList.find().fromString)

    }
}
log.debug(listOfHistory)
return listOfHistory
