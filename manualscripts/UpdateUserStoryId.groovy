package com.elm.scriptrunner.scripts

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.web.bean.PagerFilter
import com.onresolve.scriptrunner.runner.customisers.WithPlugin
import org.apache.log4j.Logger

@WithPlugin("com.elm.jira.plugia")

def log = Logger.getLogger("com.onresolve.jira.groovy")
def userManager = ComponentAccessor.getUserManager()
def issueService = ComponentAccessor.issueService
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def user = userManager.getUserByName("khalqahtani")
def jqlSearch = "project = \"JIRA DEMO\" AND issuetype = Test AND \"User Story ID\" ~ CAD-4"
def issues = findIssues(jqlSearch,user)
def bField = customFieldManager.getCustomFieldObject("customfield_12000") //field of User Story Id

//Get Issue List for JQL filter
def findIssues(String jqlSearch, ApplicationUser user) {
    def searchService = ComponentAccessor.getComponent(SearchService)
    def issueManager = ComponentAccessor.getIssueManager()
    SearchService.ParseResult parseResult =  searchService.parseQuery(user, jqlSearch)

    def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
    log.info(searchResult)
    def results = searchResult.results.collect {ComponentAccessor.getIssueManager().getIssueObject(it.id)}
    return results
}

return (issues.collect{it.key})