package com.elm.scriptrunner.scripts

import com.atlassian.applinks.api.ApplicationLinkResponseHandler
import com.atlassian.applinks.api.ApplicationLinkService
import com.atlassian.applinks.api.ApplicationType
import com.atlassian.applinks.api.application.bamboo.BambooApplicationType
import com.atlassian.sal.api.component.ComponentLocator
import com.atlassian.sal.api.net.Request
import com.atlassian.sal.api.net.Response
import com.atlassian.sal.api.net.ResponseException
import com.onresolve.scriptrunner.runner.util.UserMessageUtil
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import org.apache.log4j.Logger
log = Logger.getLogger("com.atlassian.sclogs")




//############### Edit the Values here#############
readPermission = ["READ"] // Do not change any thing
writePermissionDeploymentPlan = ["READ", "WRITE"] // Do not change any thing
writePermissionBuildPlan = ["READ", "BUILD", "WRITE"]// Do not change any thing
//-----------------------------------------------------
def permissionType = 'group'    //for group use the value:'group'  for single user use the value:'user'
def userName = 'DCS_Ops_Apps' // provide the group name or user here for which you need the access
def pKey
def planPermission = readPermission  // for read only access use the value 'readPermission' for write access use the value 'writePermissionBuildPlan'
def depPermission = readPermission // for read only access use the value 'readPermission' for write access use the value 'writePermissionDeploymentPlan'

//##################################################




def projectsList = appRequestCall("", BambooApplicationType, "rest/api/latest/search/projects.json?max-result=500", "get")
def projectsListParsed = new JsonSlurper().parseText(projectsList)
projectsListParsed.searchResults.each {

    pKey = it.id

    if (permissionType.toLowerCase() == 'group') {
        //Project Permission
        appRequestCall(readPermission, BambooApplicationType, "rest/api/latest/permissions/project/${pKey}/groups/${userName}", "PUT")
        //Build plans Permission
        try {
            def request = appRequestCall(planPermission, BambooApplicationType, "rest/api/latest/permissions/projectplan/${pKey}/groups/${userName}", "PUT")
            log.debug(request)
        }
        catch (Exception e) {
        }

        //deployment plans
        try {
            def planKeys = appRequestCall("", BambooApplicationType, "rest/api/1.0/project/${pKey}.json?expand=plans", "GET")
            def parsedJson = new JsonSlurper().parseText(planKeys)

            parsedJson.plans.plan.each { plansKeys ->
                String deploymentPlans = appRequestCall("", BambooApplicationType, "/rest/api/latest/deploy/project/forPlan?planKey=${plansKeys.key}", "GET")
                if (deploymentPlans != "[]") {
                    def deploymentPlanP = new JsonSlurper().parseText(deploymentPlans)
                    String deploymentPlanId = deploymentPlanP.id
                    String deploymentPlanName = deploymentPlanP.name
                    //String deploymentPlanId = new JsonSlurper().parseText(deploymentPlans).id
                    def deploymentPlanIdc = deploymentPlanId.substring(1, deploymentPlanId.length() - 1)
                    def request = appRequestCall(depPermission, BambooApplicationType, "rest/api/latest/permissions/deployment/${deploymentPlanIdc.toString()}/groups/${userName}", "PUT")
                }
            }
        }
        catch (Exception e) {
        }

    } else {
        //Project Permission
        appRequestCall(readPermission, BambooApplicationType, "rest/api/latest/permissions/project/${pKey}/users/${userName}", "PUT")


        //Build plans Permission
        try {
            def request = appRequestCall(planPermission, BambooApplicationType, "rest/api/latest/permissions/project/${pKey}/users/${userName}", "PUT")
            log.debug(request)
        }
        catch (Exception e) {
        }

        //deployment plans
        try {
            def planKeys = appRequestCall("", BambooApplicationType, "rest/api/1.0/project/${pKey}.json?expand=plans", "GET")
            def parsedJson = new JsonSlurper().parseText(planKeys)

            parsedJson.plans.plan.each { plansKeys ->
                String deploymentPlans = appRequestCall("", BambooApplicationType, "/rest/api/latest/deploy/project/forPlan?planKey=${plansKeys.key}", "GET")
                if (deploymentPlans != "[]") {
                    def deploymentPlanP = new JsonSlurper().parseText(deploymentPlans)
                    String deploymentPlanId = deploymentPlanP.id
                    String deploymentPlanName = deploymentPlanP.name
                    //String deploymentPlanId = new JsonSlurper().parseText(deploymentPlans).id
                    def deploymentPlanIdc = deploymentPlanId.substring(1, deploymentPlanId.length() - 1)
                    def request = appRequestCall(depPermission, BambooApplicationType, "rest/api/latest/permissions/deployment/${deploymentPlanIdc.toString()}/users/${userName}", "PUT")
                }
            }
        }
        catch (Exception e) {
            UserMessageUtil.error(bambooDPException + e)
        }
    }

}


def appRequestCall(permissionType, Class<? extends ApplicationType> appType, String restCallURL, String methodtype) {

    def appLinkService = ComponentLocator.getComponent(ApplicationLinkService)
    def appLinkName = appLinkService.getPrimaryApplicationLink(appType)
    // We must have a working application link set up to proceed
    assert appLinkName
    def authenticatedRequestFactory = appLinkName.createAuthenticatedRequestFactory()
    if (appLinkName.name == 'Bamboo' & methodtype.toLowerCase() == 'put') {
        def request = authenticatedRequestFactory.createRequest(Request.MethodType.PUT, restCallURL)
                .addHeader("Content-Type", "application/json")
                .setRequestBody(new JsonBuilder(permissionType).toString())
                .execute(new MyResponseHandler())
    } else if (appLinkName.name == 'Bitbucket' & methodtype.toLowerCase() == 'put') {
        def request = authenticatedRequestFactory.createRequest(Request.MethodType.PUT, restCallURL)
                .addHeader("Content-Type", "application/json")
                .execute(new MyResponseHandler())
    } else if (appLinkName.name == 'Bitbucket' & methodtype.toLowerCase() == 'get') {
        def request = authenticatedRequestFactory.createRequest(Request.MethodType.GET, restCallURL)
                .addHeader("Content-Type", "application/json")
                .execute()
    } else if (appLinkName.name == 'Wiki') {
        def request = authenticatedRequestFactory.createRequest(Request.MethodType.PUT, restCallURL)
                .addHeader("Content-Type", "application/json")
                .execute()
    } else if (appLinkName.name.toLowerCase() == 'bamboo' & methodtype.toLowerCase() == 'get') {
        authenticatedRequestFactory.createRequest(Request.MethodType.GET, restCallURL)
                .addHeader("Content-Type", "application/json")
                .execute()

    } else if (appLinkName.name.toLowerCase() == 'crowd' & methodtype.toLowerCase() == 'post') {
        def request = authenticatedRequestFactory.createRequest(Request.MethodType.POST, restCallURL)
                .addHeader("Content-Type", "application/json")
                .setRequestBody(new JsonBuilder(permissionType).toString())
                .execute(new MyResponseHandler())
    }

}


class MyResponseHandler implements ApplicationLinkResponseHandler {
    @Override
    Object credentialsRequired(Response response) throws ResponseException {
        return null
    }

    @Override
    Object handle(Response response) throws ResponseException {
        //UserMessageUtil.info("df"+response.getStatusCode())
        return response.getStatusCode()
    }
}


