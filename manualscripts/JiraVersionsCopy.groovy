package com.elm.scriptrunner.manualscripts

import com.atlassian.jira.bc.projectroles.ProjectRoleService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.project.version.Version
import groovy.transform.Field
import groovyx.net.http.RESTClient
import org.apache.log4j.Logger

import static groovyx.net.http.ContentType.JSON
@Field log = Logger.getLogger("com.onresolve.jira.groovy")


def sourceProject='JD'
def destProject='JD'

def projectManager = ComponentAccessor.getProjectManager()
def sourceProjectId=projectManager.getProjectObjByKey(sourceProject).id
def destProjectId=projectManager.getProjectObjByKey(destProject).id


def getAllVersions = ComponentAccessor.getVersionManager().getVersions(projectManager.getProjectObjByKey(sourceProject))
def versionManager = ComponentAccessor.getVersionManager()


//ComponentAccessor.getVersionManager().deleteAllVersions(14202)

// each version


getAllVersions.each {version->
  def name=""
  def description=ComponentAccessor.getVersionManager().getVersion(sourceProjectId,version.toString()).description
  name=ComponentAccessor.getVersionManager().getVersion(sourceProjectId,version.toString()).name
  def archived=ComponentAccessor.getVersionManager().getVersion(sourceProjectId,version.toString()).archived
  def released=ComponentAccessor.getVersionManager().getVersion(sourceProjectId,version.toString()).released
  def releaseDate=ComponentAccessor.getVersionManager().getVersion(sourceProjectId,version.toString()).releaseDate
  def startDate=ComponentAccessor.getVersionManager().getVersion(sourceProjectId,version.toString()).startDate




  versionManager.editVersionDetails(ComponentAccessor.getVersionManager().getVersion(version.id),"${sourceProject}-${name}","")
  ComponentAccessor.getVersionManager().createVersion(name, startDate, releaseDate, description, destProjectId, null, released)




}






