package com.elm.scriptrunner.scripts

import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger

def log = Logger.getLogger("com.onresolve.jira.groovy")
def projectManager = ComponentAccessor.getProjectManager()

def projectsList = projectManager.getProjects()


log.info(projectsList.collect {"'${it.name} ${it.key} ${it.getProjectCategory()}'"})