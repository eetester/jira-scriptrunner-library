package com.elm.scriptrunner.scripts

def generator = { String alphabet, int n ->
    new Random().with {
        (1..n).collect { alphabet[ nextInt( alphabet.length() ) ] }.join()
    }
}

def targetRepoKey = generator( (('A'..'Z')).join(), 4 )

return targetRepoKey