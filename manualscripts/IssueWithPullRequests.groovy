package com.elm.scriptrunner.scriptfields

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser
import groovy.transform.Field
import org.apache.log4j.Logger
import groovy.sql.Sql
import java.sql.Driver

@Field issue = ComponentAccessor.getIssueManager().getIssueByCurrentKey("ZAW-100")


@Field log = Logger.getLogger("com.onresolve.jira.groovy")
@Field ApplicationUser currentUser = ComponentAccessor.jiraAuthenticationContext.getLoggedInUser()
@Field ccm = ComponentAccessor.getComponentClassManager()
@Field service = ccm.newInstance("com.atlassian.jira.plugin.devstatus.impl.DefaultDevStatusSummaryService")
@Field details = service.getDetailData(issue.id, "stash", "pullrequest", currentUser).right().get().getDetail().pullRequests
@Field prStatus = ''






def getPullRequestStatus() {
    if (details[0].size() >= 1) {
        prStatus = details?.last()?.last()?.status
    } else {
        prStatus = 'UnReviewed'
    }
    return prStatus
}



getPullRequestStatus()


def getSQLConnection() {

    def driver = Class.forName('org.postgresql.Driver').newInstance() as Driver

    def props = new Properties()
    props.setProperty("user", "bot")
    props.setProperty("password", "Aa123456")

    def conn = driver.connect("jdbc:postgresql://localhost:5432/jira_6.4.6", props)
    def sql = new Sql(conn)

    try {
        sql.eachRow("select count(*) from tbl_jira_issues") {
            log.debug(it)
        }
    } finally {
        sql.close()
        conn.close()
    }
}