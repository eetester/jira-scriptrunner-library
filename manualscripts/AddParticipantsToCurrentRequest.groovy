package com.elm.scriptrunner.manualscripts

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.issue.IssueInputParameters
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Logger
import com.atlassian.jira.web.bean.PagerFilter

def userManager = ComponentAccessor.getUserManager()
def issueService = ComponentAccessor.issueService
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def user = userManager.getUserByName("mmojahed")
IssueInputParameters issueInputParameters = issueService.newIssueInputParameters()
def requestParticipantsField = customFieldManager.getCustomFieldObject("customfield_11201") //get Request Participant Object
def jqlSearch = "project = \"Atlassian Help\" AND type = \"Service Request\" AND \"Customer Request Type\" = \"Project Management Processes incidents (SUP)\""
def issues = findIssues(jqlSearch,user)

issues.each {
    issueInputParameters.addCustomFieldValue(requestParticipantsField.id,"somar,shihab")//Update Customefield Value
    def update = issueService.validateUpdate(user, it.id, issueInputParameters)
    if (update.isValid()) {
        issueService.update(user, update)
    }
}
//Get Issue List
def findIssues(String jqlSearch,ApplicationUser user) {
    def searchService = ComponentAccessor.getComponent(SearchService)
    def issueManager = ComponentAccessor.getIssueManager()
    SearchService.ParseResult parseResult =  searchService.parseQuery(user, jqlSearch)
    def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
    def results = searchResult.issues.collect {issueManager.getIssueObject(it.id)}
    return results
}