package com.elm.scriptrunner.manualscripts

import com.atlassian.jira.bc.projectroles.ProjectRoleService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.security.roles.ProjectRoleActor
import com.atlassian.jira.security.roles.ProjectRoleManager
import com.atlassian.jira.util.SimpleErrorCollection
import com.atlassian.sal.api.net.Request
import com.atlassian.sal.api.net.ResponseException
import com.elm.scriptrunner.library.DoRequestCall
import com.onresolve.scriptrunner.runner.customisers.WithPlugin
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovy.transform.Field
import org.apache.log4j.Logger

@WithPlugin("com.elm.jira.plugia")


def log = Logger.getLogger("com.onresolve.jira.groovy")
@Field wiki = "Wiki"
@Field bamboo = "Bamboo"
@Field jira = "Jira"
@Field bitbucket = "Bitbucket"

//@Field issue = ComponentAccessor.getIssueManager().getIssueByCurrentKey("SUP-3359")

def projectManager = ComponentAccessor.getProjectManager()
def projectRoleService = ComponentAccessor.getComponent(ProjectRoleService)
def projectRoleManager = ComponentAccessor.getComponent(ProjectRoleManager)
def errorCollection = new SimpleErrorCollection()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def sourceProjectKeys = ["CAM"]
def destProjectKey = "CA"

//copyBambooUsers(bamboo, destProjectKey, sourceProjectKeys.get(0), log)

sourceProjectKeys.each { sourceProjectKey ->

     copyJiraUsers(projectRoleManager, projectManager, sourceProjectKey, log, projectRoleService, destProjectKey, errorCollection)
    //copyBitbucketUsers(bitbucket,sourceProjectKey,destProjectKey,log)
   //  copyBambooUsers(bamboo,destProjectKey,sourceProjectKey,log)


}


static copyJiraUsers(def projectRoleManager, def projectManager, def sourceProjectKey, def log, def projectRoleService, def destProjectKey, def errorCollection) {
    def projectRoles = projectRoleManager.getProjectRoles()
    projectRoles.each { pRole ->
        def projectRoleActors = projectRoleManager.getProjectRoleActors(pRole, projectManager.getProjectObjByKey(sourceProjectKey))
        projectRoleActors.each {
            def projectRolesWithActos = it.getRoleActors()
            projectRolesWithActos.each {
                projectRoleService.addActorsToProjectRole([it.parameter], projectRoleManager.getProjectRole(it.projectRoleId)
                        , projectManager.getProjectObjByKey(destProjectKey), ProjectRoleActor.USER_ROLE_ACTOR_TYPE, errorCollection)

            }
        }
    }
}

static copyBitbucketUsers(def bitbucket, def sourceProjectKey, def destProjectKey, def log) {
    try {
        def getbitBucketUsers = DoRequestCall.doCall("", bitbucket,
                "/rest/api/1.0/projects/${sourceProjectKey}/permissions/users?limit=300",
                Request.MethodType.GET)
        def getbitBucketUsersP = new JsonSlurper().parseText(getbitBucketUsers.toString())
        //log.debug(getbitBucketUsersP)
        getbitBucketUsersP.values.each {

            def userName = it.user.name
            def permission = it.permission
            def bitbucketRestCallAll = "/rest/api/1.0/projects/${destProjectKey}/permissions/users?name=${userName}&permission=${permission}"
            DoRequestCall.doCall("", bitbucket, bitbucketRestCallAll, Request.MethodType.PUT)
        }
    }
    catch (Exception e) {
        throw e
    }


}

static copyBambooUsers(def bamboo, def destProjectKey, def sourceProjectKey, def log) {

    def readPermissions = new JsonBuilder(["READ"]).toString()


    //START####################Copy permissions to Project and Build Plans##################
    try {
        def getBambooUsers = DoRequestCall.doCall("", bamboo,
                "rest/api/latest/permissions/projectplan/${sourceProjectKey}/users?limit=500",
                Request.MethodType.GET)
        def getBambooUsersP = new JsonSlurper().parseText(getBambooUsers.toString())
        getBambooUsersP.results.each {
            def userName = it.name
            def permissions = it.permissions

            //copy users to bamboo projects
            try {
                DoRequestCall.doCall(readPermissions, bamboo,
                        "rest/api/latest/permissions/project/${destProjectKey}/users/${userName}?limit=500",
                        Request.MethodType.PUT)
            } catch (ResponseException e) {
            }

        }
    }
    catch (Exception e) {

        throw e
    }
    //END####################Copy permissions to Project and Build Plans##################


    //START####################Copy permissions to Project Deployment Plans##################

    try {
        def planKeys = DoRequestCall.doCall("",
                bamboo,
                "rest/api/1.0/project/${sourceProjectKey}.json?expand=plans&max-result=500",
                Request.MethodType.GET)

        def parsedJson = new JsonSlurper().parseText(planKeys.toString())
        parsedJson.plans.plan.each { plansKeys ->
            //    log.debug("plan: ${plansKeys.key}")
            String deploymentPlans = DoRequestCall.doCall("", bamboo,
                    "/rest/api/latest/deploy/project/forPlan?planKey=${plansKeys.key}",
                    Request.MethodType.GET)

            if (deploymentPlans != "[]") {
                def deploymentPlanP = new JsonSlurper().parseText(deploymentPlans)
                String deploymentPlanId = deploymentPlanP.id
                String deploymentPlanName = deploymentPlanP.name
                def deploymentPlanIdc = deploymentPlanId.substring(1, deploymentPlanId.length() - 1)


                def getBambooDepPUsers = DoRequestCall.doCall("", bamboo,
                        "rest/api/latest/permissions/deployment/${deploymentPlanIdc}/users?limit=500",
                        Request.MethodType.GET)

                log.debug(getBambooDepPUsers)
                def getBambooDepPUsersP = new JsonSlurper().parseText(getBambooDepPUsers.toString())

                getBambooDepPUsersP.results.each {
                    def userName = it.name
                    //log.debug(userName)

                    def permissions = new JsonBuilder(it.permissions).toString()
                    //log.debug(destProjectKey)
                    bambooDeploymentPlans(destProjectKey, permissions, userName, bamboo, log)

                }

            }
        }
    }
    catch (Exception e) {
        // throw e
    }
    //END####################Copy permissions to Project Deployment Plans##################


}

static bambooDeploymentPlans(def destProjectKey, def permissions, def reportUser, def bamboo, def log) {

    try {
        def planKeys = DoRequestCall.doCall("", bamboo, "rest/api/1.0/project/${destProjectKey}.json?expand=plans&max-result=500", Request.MethodType.GET)
        def parsedJson = new JsonSlurper().parseText(planKeys)
        parsedJson.plans.plan.each { plansKeys ->
            String deploymentPlans = DoRequestCall.doCall("", bamboo,
                    "/rest/api/latest/deploy/project/forPlan?planKey=${plansKeys.key}",
                    Request.MethodType.GET)


            if (deploymentPlans != "[]") {
                def deploymentPlanP = new JsonSlurper().parseText(deploymentPlans)
                String deploymentPlanId = deploymentPlanP.id
                def deploymentPlanIdc = deploymentPlanId.substring(1, deploymentPlanId.length() - 1)
                try {
                    DoRequestCall.doCall(permissions, bamboo,
                            "rest/api/latest/permissions/deployment/${deploymentPlanIdc.toString()}/users/${reportUser}?limit=100",
                            Request.MethodType.PUT)
                } catch (Exception e) {
                    //throw e
                }
            }
        }
    }
    catch (ResponseException e) {
        // throw e
    }
    catch (Exception e) {
        // throw e
    }
}

