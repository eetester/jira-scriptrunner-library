import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.security.roles.ProjectRoleManager
import com.atlassian.mail.Email
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService
import groovy.transform.Field
import org.apache.log4j.Logger


@Field def log = Logger.getLogger("com.atlassian.sclogs")

def emailBody =
        "<body>" +
                "<h3>This to inform you that a new space: ${destProjectName} is created in WIKI:</h3>" +
                "<p> you can access the projec by url: https://wiki.elm.sa/display/$destProjectKey/Architecture </p>" +
                "<p> If you cannot access the project, please raise a service request: https://jira.elm.sa/servicedesk/customer/portal/1/create/10 </p>" +
                "</br>" +
                "</br>" +
                "<p>Regards,</p>" +
                "<p>Tech Affairs ALM Support | techsupport@elm.sa,</p>" +

                "</br>" +
                "</br>" +


                "<p style=\"color: #731768;\">This is an auto email sent by JIRA, You are receiving this email because you are part of the group:Tech_Arch@elm.sa</p>" +

                "</body>"
def sendJiraEmail=sendEmailToJiraGroup("atlassian_admin_users","Muahmad@elm.sa,mmojahed@elm.sa, khalqahtani@elm.sa", ":WIKI Space is Created", emailBody)


//##########Project Role##################################################
def sendEmailtoJiraProjectRole(def jiraProjectrole, def jiraProjectKey, def subject, def ccEmail, def emailBody) {
    def projectRoleManager = ComponentAccessor.getComponent(ProjectRoleManager)
    def projectRole = projectRoleManager.getProjectRole(jiraProjectrole)
    def projectManager = ComponentAccessor.getProjectManager()
    def jiraProject = projectManager.getProjectObjByKey(jiraProjectKey)
    def projectRoleActors = projectRoleManager.getProjectRoleActors(projectRole, jiraProject)
    def projectRoleActorsEmailTo = projectRoleActors?.getApplicationUsers()*.emailAddress.join(",")
    sendEmailtoUsers(projectRoleActorsEmailTo, ccEmail, subject, emailBody)

}
//##########Project Role##################################################


//##################JIRA Groups##############################################
def sendEmailToJiraGroup(def jiraGroupvalue, def ccEmail, def subject, def emailBody) {
    def jiragroupActorsTo
    def groupManager = ComponentAccessor.getGroupManager()
    def jiraGroup = groupManager.getGroup(jiraGroupvalue)
    def jiraGroupActors = groupManager.getUsersInGroup(jiraGroup)
    def j = 0
    while (j < jiraGroupActors.size()) {
        jiragroupActorsTo = jiragroupActorsTo + "," + jiraGroupActors[j].getEmailAddress()
        j++
    }
    sendEmailtoUsers(jiragroupActorsTo - null, ccEmail, subject, emailBody)
}
//##################JIRA Groups##############################################


//##################Send email to users##############################################

def sendEmailtoUsers(def emailAddress/*Provide email as single email or multiple emails for example email1@emali.com,email2@email.com*/, def ccemail, def subject, def body) {
    try {
        def mailServer = ComponentAccessor.getMailServerManager().getDefaultSMTPMailServer()
        if (emailAddress != '' || emailAddress != null) {
            if (mailServer) {
                Email email = new Email(emailAddress)
                email.setMimeType("text/html")
                email.setSubject(subject)
                email.setCc(ccemail)
                email.setBody(body)
                mailServer.send(email)
            } else {
                log.debug("error occured")
            }
        } else {
            log.debug("Email Id is not valid")

        }
    } catch (Exception e) {
        log.debug("Error occurred while sending email \n" + e)
    }
}
//##################Send email to users##############################################


}
