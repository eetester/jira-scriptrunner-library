package com.elm.scriptrunner.manualscripts

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.web.bean.PagerFilter
import com.onresolve.scriptrunner.db.DatabaseUtil
import org.apache.log4j.Logger

//MutableIssue issue = ComponentAccessor.getIssueManager().getIssueByCurrentKey("JD-687")
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def testSteps = customFieldManager.getCustomFieldObject(13207)
def testResults = customFieldManager.getCustomFieldObject(13301)
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def issueManager = ComponentAccessor.getIssueManager()
def jqlSearch = "issuetype =test and createdDate >\"2019-09-01\""

def getTestCases = findIssues(jqlSearch, user)
getTestCases.each {
    def allSteps = ""
    def allResults = ""
    MutableIssue issue = issueManager.getIssueByCurrentKey(it.getKey())
    getTestCaseSteps(it.id).each {
        def Step = it.STEP
        def Result = it.RESULT
        def data = it.DATA
        def orderId = it.ORDER_ID
        allSteps += "${orderId} : ${Step} (Test Data: ${data})\n"

        allResults += "${orderId} :${Result}\n"
        logs(allResults)
    }
    issue.setCustomFieldValue(testSteps, allSteps.toString())
    issue.setCustomFieldValue(testResults, allResults.toString())
    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)

}


def logs(logs) {
    def log = Logger.getLogger("com.onresolve.jira.groovy")
    log.debug(logs)

}

def getTestCaseSteps(def issueId) {
    def nProjects = DatabaseUtil.withSql('jiraprod') { sql ->
        sql.rows(
                "select \n" +
                        "issue.id,\n" +
                        "steps.ORDER_ID,\n" +
                        "issue.SUMMARY,\n" +
                        "issue.[DESCRIPTION],\n" +
                        "steps.STEP,\n" +
                        "steps.RESULT,\n" +
                        "steps.[DATA],\n" +
                        "steps.ORDER_ID\n" +
                        "from jiraissue as issue INNER join ao_7deabf_teststep as steps On issue.ID=steps.ISSUE_ID\n" +
                        "\n" +
                        "where  issue.issuetype=10100 and issue.id=${issueId}"
        )
    }
    return nProjects


}

def static findIssues(String jqlSearch, ApplicationUser user) {
    def searchService = ComponentAccessor.getComponent(SearchService)
    def issueManager = ComponentAccessor.getIssueManager()
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
    //  def results = searchResult.results.id
    def results = searchResult.results
    return results
}