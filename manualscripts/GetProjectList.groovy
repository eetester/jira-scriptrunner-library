package com.elm.scriptrunner.scripts

import com.atlassian.jira.component.ComponentAccessor


def projectManager = ComponentAccessor.getProjectManager()
//def projectsList = projectManager.getProjectObjectsFromProjectCategory(10002)
def projectsList = projectManager.getProjects()


projectsList.count {
    sprintf("%32s%10s%16s", it.key, it.name, it.leadUserName)
}