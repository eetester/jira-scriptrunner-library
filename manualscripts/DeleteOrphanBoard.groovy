package com.elm.scriptrunner.manualscripts

import com.atlassian.greenhopper.service.rapid.view.RapidViewService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser
import com.onresolve.scriptrunner.canned.jira.utils.plugins.RapidBoardUtils
import com.onresolve.scriptrunner.runner.customisers.JiraAgileBean
import com.onresolve.scriptrunner.runner.customisers.WithPlugin
import groovy.sql.Sql
import org.ofbiz.core.entity.ConnectionFactory
import org.ofbiz.core.entity.DelegatorInterface

import java.sql.Connection

@WithPlugin("com.pyxis.greenhopper.jira")

@JiraAgileBean
RapidViewService rapidViewService


def rapidView = new RapidBoardUtils()
ApplicationUser botuser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def views = rapidViewService.getRapidViews(user)

def delegator = (DelegatorInterface) ComponentAccessor.getComponent(DelegatorInterface)
String helperName = delegator.getGroupHelperName("default");


def sqlProj ="""
select pname from project;
"""

// need use different table, because this table keep actual keys. In previus table may exist old keys if project change key.
def sqlProjKey="""
select PROJECT_KEY from project_key;
"""

// get all boards where filters have word project
def sqlStmt = """
SELECT r.ID, s.reqcontent FROM AO_60DB71_RAPIDVIEW as r inner join searchrequest as s on r.saved_filter_id = s.id where s.reqcontent like '%project = %';
"""
def projNames=[]
def projKeys=[]

def rapids = []

Connection conn = ConnectionFactory.getConnection(helperName);
Sql sql = new Sql(conn)

try {
    // get all projects names
    sql.eachRow(sqlProj){
        projNames.add(it.pname.trim())
    }

    // get all pojects keys
    sql.eachRow(sqlProjKey){
        projNames.add(it.PROJECT_KEY.trim())
    }

    sql.eachRow(sqlStmt) {

        if (it.reqcontent ==~ /^((?!\sAND\s|\sand\s|\sOR\s|\sor\s).)*$/){
            if (it.reqcontent ==~ /.*project =.*/){
                def bufStr = it.reqcontent.split('=')[1].split('ORDER')[0]

                def projName
                projName = bufStr.replaceAll(/"/, "").trim()
                def notF = true
                if (projNames.find{projName == it}){
                    notF = false

                }
                if (projKeys.find{projName == it}){
                    notF = false
                }
                if(notF){
                    rapids.add(it.id)
                }
            }
        }
    }
}
finally {
    sql.close()
}

rapids.each {
    rapidView.deleteRapidView(botuser, view)
}


return rapids