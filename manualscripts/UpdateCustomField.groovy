package com.elm.scriptrunner.manualscripts

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.IssueInputParameters
import com.atlassian.jira.project.version.Version
import com.elm.jira.library.CommonUtil
import com.onresolve.scriptrunner.runner.customisers.WithPlugin
import org.apache.log4j.Logger


@WithPlugin("com.elm.jira.plugia")

///**Update customFieldValue with UpdateIssue**/
//def log = Logger.getLogger("com.onresolve.jira.groovy")
//def issue = ComponentAccessor.issueManager.getIssueByCurrentKey("MS-83")
//def userManager = ComponentAccessor.getUserManager()
//def customField = ComponentAccessor.getCustomFieldManager().getCustomFieldObject(10707)
//def issueManager = ComponentAccessor.getIssueManager()
//def user = userManager.getUserByName("YourAdmin")
//def customFieldValue = issue.key +" issueService "+ issue.summary
//
//
//issue.setCustomFieldValue(customField,customFieldValue )
//def versionManager = ComponentAccessor.getVersionManager()
//def versions = versionManager.getVersionsByName("None")
//issue.setFixVersions(versions)
//
////ImportantUpdate Issue for Lower level doesn't require any permission
//issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

def updateCustomFieldWithIssueService(issue,def customFieldObject,def customFieldValue) {

/**Update customFieldValue with IssueService**/
    def issueService = ComponentAccessor.issueService
    def customfield = CommonUtil.getCustomFieldObject(11600)
    IssueInputParameters issueInputParameters = issueService.newIssueInputParameters()
    issueInputParameters.addCustomFieldValue(customFieldObject.id, customFieldValue.toString())//Update Customefield Value
    def update = issueService.validateUpdate(user, issue.id, issueInputParameters)
    if (update.isValid()) {
        issueService.update(user, update)
    }
}

