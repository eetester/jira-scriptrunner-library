package com.elm.scriptrunner.scripts

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue

def workflowManager = ComponentAccessor.workflowManager
def schemeManager = ComponentAccessor.workflowSchemeManager

def sb = new StringBuffer()

MutableIssue issue

workflowManager.workflows.each {
    if(!it.systemWorkflow) {
        def schemes = schemeManager.getSchemesForWorkflow(it)
        if (schemes.size() == 0) {
            sb.append("Deleting workflow: ${it.name}\n")
            workflowManager.deleteWorkflow(it)
        }
    }
}

return sb.toString()


if (issue.issueType.name == "Bug") {
    issue.setAssignee(issue.reporterUser)
}


issue.setComponent()

if (issue.issueType.name == "sub-task") {
    issue.setComponent(issue.getParentObject().getComponents())
    issue.getComponents()
}
issue.setComponent(issue.getParentObject().getComponents())
