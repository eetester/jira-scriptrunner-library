package com.elm.scriptrunner.scripts

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.web.bean.PagerFilter
import com.onresolve.scriptrunner.canned.jira.workflow.postfunctions.CloneIssue
import org.apache.log4j.Logger

def userManager = ComponentAccessor.getUserManager()
def user = userManager.getUserByName("mmojahed")
def jqlSearch = "project = \"Reusable Test Library\""
def issues = findIssues(jqlSearch,user)
//def originalIssueKey = issueManager.getIssueByCurrentKey("ATG-1")
def destinationProjectKey = ComponentAccessor.projectManager.getProjectObjByKey("EJR")

for(issue in issues){
    try {
        cloneIssue(issue, destinationProjectKey)
    }
    catch (Exception e) {
        throw e
    }
}

def cloneIssue (MutableIssue issue, Project project) {
    def params = [
        issue                                   : issue,
        (CloneIssue.FIELD_TARGET_PROJECT)       : project.key,
        (CloneIssue.FIELD_SELECTED_FIELDS)      : null, //clone all the fields
    ] as Map<String, Object>
    myLogger(params)
    new CloneIssue().doScript(params)
}

//Get Issue List
def findIssues(String jqlSearch, ApplicationUser user) {
    def searchService = ComponentAccessor.getComponent(SearchService)
    def issueManager = ComponentAccessor.getIssueManager()
    SearchService.ParseResult parseResult =  searchService.parseQuery(user, jqlSearch)
    def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
    def results = searchResult.results.collect {issueManager.getIssueObject(it.id)}
    return results
}

def myLogger (defLogObject){
    def log = Logger.getLogger("com.onresolve.jira.groovy")
    log.debug(defLogObject)
}