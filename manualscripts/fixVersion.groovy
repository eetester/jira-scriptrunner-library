package com.elm.scriptrunner.manualscripts

import com.atlassian.jira.component.ComponentAccessor

import java.time.LocalDateTime


def deleteFixVersion(long projectId){
    def versionManager = ComponentAccessor.getVersionManager()
    versionManager.deleteAllVersions(projectId)
}

def updateFixVersionName(long projectId) {

    def versionManager = ComponentAccessor.getVersionManager()
    def versionList = versionManager.getVersions(projectId)
    versionList.find { version -> version.name.startsWith('08.') }.each {
        def versoinObject = versionManager.getVersionsByName(it.name).first()
        versionManager.editVersionDetails(versoinObject, it.name.replace('08.', 'CAE_08.'), '')
        versionManager.update(versoinObject)
    }
}

def addFixVersion(long projectId){
    def versionManager = ComponentAccessor.getVersionManager()
    versionManager.createVersion('', LocalDateTime.now() as Date, LocalDateTime.now() as Date,'',projectId, '' as Long,false)
}