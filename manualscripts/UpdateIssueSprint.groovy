package com.elm.scriptrunner.manualscripts

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.IssueInputParameters
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Logger
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.greenhopper.service.sprint.SprintService
import com.onresolve.scriptrunner.runner.customisers.WithPlugin
import com.onresolve.scriptrunner.runner.customisers.JiraAgileBean


@WithPlugin("com.pyxis.greenhopper.jira")

@JiraAgileBean
SprintService sprintService
def log = Logger.getLogger("com.onresolve.jira.groovy")
def userManager = ComponentAccessor.getUserManager()
def issueService = ComponentAccessor.issueService
def user = userManager.getUserByName("mmojahed")
def sprint = sprintService.getSprint(user,367)
log.debug(sprint.value)
IssueInputParameters issueInputParameters = issueService.newIssueInputParameters()
def jqlSearch = "project = SEEC AND issue in (SEEC-1259, SEEC-1260,SEEC-1261,SEEC-1262,SEEC-1263,SEEC-1294,SEEC-1295,SEEC-1296)"
def issues = findIssues(jqlSearch,user)
def customField =  ComponentAccessor.getCustomFieldManager().getCustomFieldObject(10004)
log.debug(customField)
//def sprint = sprintServiceOutcome.getValue().find {it.name == "Mojaz Mobile Sprint 16"}

//Update fixversion & to update any other filed play with the issue set functions

for (issue in issues){
    def issueManager = ComponentAccessor.getIssueManager()
    def versionManager = ComponentAccessor.getVersionManager()
    //def versions = versionManager.getVersionsByName("0.0.1.R")
    issue.setCustomFieldValue(customField, sprint)//Update fixVersion Value
    //ImportantUpdate Issue for Lower level doesn't require any permission
    issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
}
//Get Issue List
def findIssues(String jqlSearch,ApplicationUser user) {
    def searchService = ComponentAccessor.getComponent(SearchService)
    def issueManager = ComponentAccessor.getIssueManager()
    SearchService.ParseResult parseResult =  searchService.parseQuery(user, jqlSearch)
    def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
    def results = searchResult.results.collect {issueManager.getIssueObject(it.id)}
    return results
}