package com.elm.scriptrunner.manualscripts

import com.atlassian.jira.component.ComponentAccessor

def projectManager = ComponentAccessor.projectManager
def projects = projectManager.getProjects()

projects.each {
    //def createComponents = ComponentAccessor.projectComponentManager.create("${it.key}","${it}",null,3,15102)
    /***To Delete a Specific Component uncomment the below line of code / Be sure no issue linked with the component***/
    def deleteComponents = ComponentAccessor.projectComponentManager.delete(15102)
}
