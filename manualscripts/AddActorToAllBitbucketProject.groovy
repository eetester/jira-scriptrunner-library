package com.elm.scriptrunner.manualscripts

import com.atlassian.sal.api.component.ComponentLocator
import com.atlassian.sal.api.net.Request
import com.atlassian.sal.api.net.Response
import com.atlassian.sal.api.net.ResponseException
import com.atlassian.sal.api.net.ResponseHandler
import com.elm.scriptrunner.library.CommonUtil
import groovy.json.JsonSlurper
import com.atlassian.applinks.api.ApplicationLinkService
import com.atlassian.applinks.api.application.bitbucket.BitbucketApplicationType


def mainMethod() {
    def reportUser = "internaudit"
    def bitGetProjectKeys = "/rest/api/1.0/projects"
    def appLinkService = ComponentLocator.getComponent(ApplicationLinkService)
    def appLinkName = appLinkService.getPrimaryApplicationLink(BitbucketApplicationType)
    // We must have a working application link set up to proceed
    assert appLinkName
    def pKeyResponse = []
    def limit = 200
    def pkey = []
    def authenticatedRequestFactory = appLinkName.createAuthenticatedRequestFactory()
    pKeyResponse << authenticatedRequestFactory.createRequest(Request.MethodType.GET, bitGetProjectKeys + "?limit=" + limit).execute()
    def jsonSlurper = new JsonSlurper()
    pkey = jsonSlurper.parseText(pKeyResponse.toString())
    CommonUtil.logs(pkey.values.key)

    for(x in (1..limit)) {
        
        try {
            authenticatedRequestFactory.createRequest(Request.MethodType.PUT, "/rest/api/1.0/projects/${pkey.values.key.get(0).get(x)}/permissions/users?name=${reportUser}&permission=PROJECT_READ")
                .addHeader("Content-Type", "application/json")
                .execute(new ResponseHandler<Response>() {
                @Override
                void handle(Response response) throws ResponseException {}
            })
        }
        catch (Exception e) {
            throw e
        }
    }

}
mainMethod()