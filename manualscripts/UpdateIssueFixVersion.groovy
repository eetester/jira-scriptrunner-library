package com.elm.scriptrunner.manualscripts

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.user.ApplicationUser
import groovy.transform.Field
import org.apache.log4j.Logger
import com.atlassian.jira.web.bean.PagerFilter



def log = Logger.getLogger("com.onresolve.jira.groovy")
@Field user = ComponentAccessor.getUserManager().getUserByName("mmojahed")

/**
 * Please Update the Jql Search before running the query
 **/
def jqlSearch = "project = SEEC AND issue in (SEEC-1259, SEEC-1260,SEEC-1261,SEEC-1262,SEEC-1263,SEEC-1294,SEEC-1295,SEEC-1296)"
def issues = findIssues(jqlSearch,user)

for (issue in issues){
    def issueManager = ComponentAccessor.getIssueManager()
    def versionManager = ComponentAccessor.getVersionManager()
    def versions = versionManager.getVersionsByName("2.2.8")
    issue.setFixVersions(versions)
    issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
}
//Get Issue List
def findIssues(String jqlSearch,ApplicationUser user) {
    def searchService = ComponentAccessor.getComponent(SearchService)
    def issueManager = ComponentAccessor.getIssueManager()
    SearchService.ParseResult parseResult =  searchService.parseQuery(user, jqlSearch)
    def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
    def results = searchResult.results.collect {issueManager.getIssueObject(it.id)}
    return results
}