package com.elm.scriptrunner.manualscripts

import com.atlassian.jira.bc.projectroles.ProjectRoleService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.security.roles.ProjectRoleActor
import com.atlassian.jira.security.roles.ProjectRoleManager
import com.atlassian.jira.util.SimpleErrorCollection

int i = 0

def projectRoleManager = ComponentAccessor.getComponent(ProjectRoleManager)
def errorCollection = new SimpleErrorCollection()
def projectManager = ComponentAccessor.getProjectManager()
def projectRoleService = ComponentAccessor.getComponent(ProjectRoleService)
def jiraProject = projectManager.getProjectObjByKey("EQAP")
def reportUser1 = ['aelsayed',
                   'aalzandi',
                   'aalmusned',
                   'aoalqahtani',
                   'aalomary',
                   'abalhelal',
                   'amaldossari',
                   'asalshubaili',
                   'asaalharbi',
                   'amalessa',
                   'amaljadhee',
                   'afarhan',
                   'aotman',
                   'ahaja',
                   'bsafi',
                   'baalzahrani',
                   'falsabhan',
                   'falrammah',
                   'fmaalqahtani',
                   'fsalamah',
                   'halmaleki',
                   'hhussein',
                   'lvijayakumar',
                   'mjakangeerkh',
                   'melhafi',
                   'mazenalo',
                   'mabdullatif',
                   'malkamary',
                   'mnooghukajam',
                   'mvahmed',
                   'mbajabaa',
                   'mmubeen',
                   'nelsarsoubi',
                   'oalmunajem',
                   'oalzogady',
                   'PKunapareddy',
                   'salbraikan',
                   'sgulay',
                   'siyyankan',
                   'ssaminathan',
                   'sdevarajulu',
                   'snazeerahamm',
                   'smemon',
                   'smansoor',
                   'Tmohammad',
                   'vrengaraju',
                   'VBonda']
reportUser1.each {
    def reportUser = [reportUser1.get(i)]
    def projectRole = projectRoleManager.getProjectRole('Test Engineer')
    projectRoleService.addActorsToProjectRole(reportUser, projectRole, jiraProject, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, errorCollection)
    i++
}


