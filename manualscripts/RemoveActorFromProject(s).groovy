package com.elm.scriptrunner.scripts

import com.atlassian.jira.bc.projectroles.ProjectRoleService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.security.roles.ProjectRoleActor
import com.atlassian.jira.security.roles.ProjectRoleManager
import com.atlassian.jira.util.SimpleErrorCollection
import org.apache.log4j.Logger

def projectManager = ComponentAccessor.getProjectManager()
def projectRoleService = ComponentAccessor.getComponent(ProjectRoleService)
def projectRoleManager = ComponentAccessor.getComponent(ProjectRoleManager)
def errorCollection = new SimpleErrorCollection()
def log = Logger.getLogger("com.onresolve.jira.groovy")

def project = projectManager.getProjectObjByKey("GSP")
def projectRole = projectRoleManager.getProjectRoles().find{it-> it.name == "Developers"}
def userLowercase = "KALHERAISI".toLowerCase() //Remove Lower Case When Remove User
def actors = [userLowercase]
Collection<String> actorCollection = new ArrayList<>()
//def actorGroups = ["Production_problem","Operation Mgrs","Performance_team","Security_Team","dev_audit"]

//projectRoleService.removeActorsFromProjectRole(actors, projectRole, project, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, errorCollection)
projectRoleService.addActorsToProjectRole(actors, projectRole, project, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, errorCollection)
//projectRoleService.removeActorsFromProjectRole(actorGroups, projectRole, project, ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE, errorCollection)