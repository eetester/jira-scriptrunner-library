package com.elm.scriptrunner.manualscripts


import com.atlassian.greenhopper.service.rapid.view.RapidViewService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser
import com.onresolve.scriptrunner.canned.jira.utils.plugins.RapidBoardUtils
import com.onresolve.scriptrunner.runner.customisers.JiraAgileBean
import com.onresolve.scriptrunner.runner.customisers.WithPlugin
import org.apache.log4j.Logger

@WithPlugin("com.pyxis.greenhopper.jira")

@JiraAgileBean
RapidViewService rapidViewService


/*======Delete First then Clone ====*/

ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
//def projectList = ComponentAccessor.getProjectManager().getProjectByCurrentKey("ZAW")
def projectList = ComponentAccessor.getProjectManager().getProjects()

def log = Logger.getLogger("com.onresolve.jira.groovy")

def rapidView = new RapidBoardUtils()
//def view = rapidViewService.getRapidView(user, 108 as long).value

def views = rapidViewService.getRapidViews(user)

log.debug(views.value.size())

projectList.each { project ->
    //if(project.key == "ZAW") {
    //views.value.each { view ->
    rapidView.deleteRapidView(user, view)

    //project.components.each {
    if(!project.components) {
        rapidView.cloneRapidBoard(project, 'TEMPKAMP', it.name + '_Board')
        //  }
        //}
    }
    //}
}