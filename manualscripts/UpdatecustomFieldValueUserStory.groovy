package com.elm.scriptrunner.manualscriptspackage
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.link.IssueLinkTypeManager
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.web.bean.PagerFilter
import org.apache.log4j.Logger



//def issue = ComponentAccessor.getIssueManager().getIssueByCurrentKey("CA-3241")



def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def jqlSearch = "project = CA AND \"User Story ID\" !~  CA-924"
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def issueManager = ComponentAccessor.getIssueManager()
def userStoryId = customFieldManager.getCustomFieldObject(12000)






def oldKeys=findIssues(jqlSearch, user)
oldKeys.each {
    MutableIssue issue=issueManager.getIssueObject(it.id)
    def oldKey=it.getCustomFieldValue(userStoryId)
    def newKeys=findIssues("key=${oldKey}", user)
    logs(it.key+" : "+ oldKey+ " : "+newKeys.get(0).key)

    issue.setCustomFieldValue(userStoryId, newKeys.get(0).key.toString())
    issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)



}











def static findIssues(String jqlSearch, ApplicationUser user) {
    def searchService = ComponentAccessor.getComponent(SearchService)
    def issueManager = ComponentAccessor.getIssueManager()
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
    //def results = searchResult.results.collect {issueManager.getIssueObject(it.id)}
    def results = searchResult.results
    return results
}



def logs(logs) {
    def log = Logger.getLogger("com.onresolve.jira.groovy")
    log.debug(logs)


}