package com.elm.scriptrunner.manualscripts

import com.atlassian.jira.component.ComponentAccessor
import groovy.util.logging.Log4j
import com.opensymphony.workflow.InvalidInputException

def linkMgr = ComponentAccessor.getIssueLinkManager()
def linkCollections = linkMgr.getLinkCollectionOverrideSecurity(issue).getAllIssues()

for (link in linkCollections) {
    if (link.issueType.name == "Bug" && !(link.status.name in ['Done','Deferred'])) {
        def invalidInputException = new InvalidInputException("You need to take an action (Close/Defer) for all linked Bugs : "
            + linkCollections.findAll{it.issueType.name == "Bug" && !(it.status.name in ['Done','Deferred'])})
        //log.debug("Please Close all linked Bugs")

        throw invalidInputException
    }
    else if(link.issueType.name != "Test"){
        def invalidInputException = new InvalidInputException("Test Cases should be linked with Business Stories")
        //log.debug("Please Close all linked Bugs")
        throw invalidInputException
    }
}