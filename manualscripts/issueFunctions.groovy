package com.elm.scriptrunner.manualscripts

import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.IssueInputParameters
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.web.bean.PagerFilter
import com.elm.jira.library.CommonUtil
import com.onresolve.scriptrunner.runner.customisers.WithPlugin
import org.apache.log4j.Logger

@WithPlugin("com.elm.jira.plugia")


def log = Logger.getLogger("com.onresolve.jira.groovy")

/*** JQL example
 *  def jqlSearch = "project = \"Atlassian Help\" AND type = \"Service Request\" AND \"Customer Request Type\" = \"Project Management Processes incidents (SUP)\""
 */

def mainMethod(){
    return (findIssues(jqlSearch, CommonUtil.executeScriptWithAdmin()))
}


def getLinkedIssues(issue){
    def linkMgr = ComponentAccessor.getIssueLinkManager()
    def linkCollections = linkMgr.getLinkCollectionOverrideSecurity(issue).getAllIssues()
    return linkCollections
}

/**Update customFieldValue with UpdateIssue**/
def updateCustomFieldValye(issue) {
    //def issue = ComponentAccessor.issueManager.getIssueByCurrentKey("MS-83")
    def customField = CommonUtil.getCustomFieldObject(10707)
    def issueManager = ComponentAccessor.getIssueManager()
    def user = userManager.getUserByName("YourAdmin")
    def customFieldValue = issue.key + " issueService " + issue.summary


    issue.setCustomFieldValue(customField, customFieldValue)
    def versionManager = ComponentAccessor.getVersionManager()
    def versions = versionManager.getVersionsByName("None")
    issue.setFixVersions(versions)

//ImportantUpdate Issue for Lower level doesn't require any permission
    issueManager.updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)
}

def updateCustomFieldWithIssueService(issue,def customFieldObject,def customFieldValue) {

/**Update customFieldValue with IssueService**/
    def issueService = ComponentAccessor.issueService
    IssueInputParameters issueInputParameters = issueService.newIssueInputParameters()
    issueInputParameters.addCustomFieldValue(customFieldObject.id, customFieldValue.toString())//Update Customefield Value
    def update = issueService.validateUpdate(user, issue.id, issueInputParameters)
    if (update.isValid()) {
        issueService.update(user, update)
    }
}

//Get Issue List for JQL filter
def findIssues(String jqlSearch, ApplicationUser user) {
    def searchService = ComponentAccessor.getComponent(SearchService)
    SearchService.ParseResult parseResult =  searchService.parseQuery(user, jqlSearch)
    def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
    def results = searchResult.results.collect {ComponentAccessor.getIssueManager().getIssueObject(it.id)}
    return results
}

mainMethod()